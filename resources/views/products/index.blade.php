@extends('layouts.app')

@section('main')
    <main>
        <div id="product-list" class="row row-cols-1 row-cols-md-3 mb-3 text-center">


        </div>
    </main>
    <script type="text/javascript">
        $(document).ready(function () {
            $.ajax({
                url: '{{ route('api.product.index') }}',
                type: 'get',
                success: function (json) {
                    var html = '';

                    json.data.forEach(function (value) {
                        html += '<div class="col">';
                        html += '<div class="card mb-4 rounded-3 shadow-sm">';
                        html += '  <div class="card-header py-3">';
                        html += '    <h4 class="my-0 fw-normal">' + value.description + ' ' + value.brand + '</h4>';
                        html += '  </div>';
                        html += '  <div class="card-body">';
                        html += '    <p class="text-left"></p>';
                        html += '    <button type="button" class="w-100 btn btn-lg btn-outline-primary">Open</button>';
                        html += '  </div>';
                        html += '</div>';
                        html += '</div>';
                    });

                    $('#product-list').html(html);
                }
            });


        });
    </script>
@endsection('main')
