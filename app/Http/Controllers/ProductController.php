<?php

namespace App\Http\Controllers;


use App\Http\Requests\Product\{
    StoreRequest,
};
use App\Models\Attribute;
use App\Models\Product;
use App\Models\ProductDescription;
use Illuminate\Http\Request;

class ProductController extends Controller
{
    /**
     * Get Product list
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        /*
        $limit = $request->input('limit');
        */

        $limit = 15;

        $filters = [];

        /*
        $category_id = $request->input('category_id');
        */

        return Product::paginate($limit);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(StoreRequest $request)
    {
        $product = new Product($request->only([
            'model',
            'brand',
        ]));

        $product->save();
        /*
        if($request->has('image')){
            foreach($request->input('image') as $request_image){

            }
        }
        */

        if($request->has('attribute')){
            foreach($request->input('attribute') as $request_attribute_key => $request_attribute){
                $product->attributes()->attach(
                    $request->input('attribute.' . $request_attribute_key . '.attribute_id'),
                    [
                        'value' => $request->input('attribute.' . $request_attribute_key . '.value')
                    ],
                );
            }
        }

        if($request->has('description')){
            foreach($request->input('description') as $request_description_key => $request_description){
                $product->description()->save(new ProductDescription([
                    'title' => $request->input('description.' . $request_description_key . '.title'),
                    'description' => $request->input('description.' . $request_description_key . '.description'),
                    'language_id' => $request->input('description.' . $request_description_key . '.language_id'),
                ]));
            }
        }

        return $product;
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
