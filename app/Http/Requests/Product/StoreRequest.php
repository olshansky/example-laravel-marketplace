<?php

namespace App\Http\Requests\Product;

use Illuminate\Foundation\Http\FormRequest;

class StoreRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array<string, mixed>
     */
    public function rules()
    {
        return [
            'model' => ['required', 'string', 'min:3', 'max:64'],
            'brand' => ['required', 'string', 'min:3', 'max:64'],
            'image.*' => ['sometimes', 'integer', 'exists:images,id'],
            'description.*.language_id' => ['sometimes', 'integer', /*'exists:languages,id'*/],
            'attribute.*.attribute_id' => ['sometimes', 'integer', 'exists:attributes,id'],
        ];
    }
}
