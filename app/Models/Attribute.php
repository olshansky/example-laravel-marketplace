<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Attribute extends Model
{
    use HasFactory;

    /**
     * The attributes that should be visible in arrays.
     *
     * @var array
     */
    protected $visible  = [];

    /**
     * The attributes that should be visible in arrays.
     *
     * @var array
     */
    protected $hidden  = [
        'created_at',
        'updated_at',
        'pivot',
    ];

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'title',
    ];

    /**
     * The model's default values for attributes.
     *
     * @var array
     */
    protected $attributes = [];

    public function products()
    {
        return $this->morphedByMany(Product::class, 'attributeable');
    }
}
