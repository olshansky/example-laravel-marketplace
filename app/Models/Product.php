<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Product extends Model
{
    use HasFactory;

    /**
     * The attributes that should be visible in arrays.
     *
     * @var array
     */
    protected $visible  = [];

    /**
     * The attributes that should be visible in arrays.
     *
     * @var array
     */
    protected $hidden  = [
        'created_at',
        'updated_at',
    ];

    /**
     * The accessors to append to the model's array form.
     *
     * @var array
     */
    protected $appends = [
        'description',
        'attributes',
    ];

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'model',
        'brand',
    ];

    /**
     * The model's default values for attributes.
     *
     * @var array
     */
    protected $attributes = [];

    public function attributes(){
        return $this->morphToMany(Attribute::class, 'attributeable');
    }

    public function description(){
        return $this->hasOne(ProductDescription::class);
    }

    public function descriptions(){
        return $this->hasMany(ProductDescription::class);
    }

    public function getDescriptionAttribute(){
        return $this->description()->where('language_id', 1)->first();
    }

    public function getAttributesAttribute(){
        return $this->attributes()->get();
    }
}
